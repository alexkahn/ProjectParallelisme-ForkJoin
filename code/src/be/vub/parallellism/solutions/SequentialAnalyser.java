package be.vub.parallellism.solutions;


import be.vub.parallellism.data.models.Comment;
import be.vub.parallellism.data.readers.RedditCommentLoader;
import com.vader.sentiment.analyzer.SentimentAnalyzer;
import com.vader.sentiment.util.ScoreType;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SequentialAnalyser {

    public static float analyse(List<Comment> comments) {
        float totalCompoundScore = 0;
        try {
            SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();
            // Multiple files can be provided. These will be read one after the other.


            // If you want to sequentially fitler the data based on the comment (to reduce a data set), use a lambda
            // List<Comment> comments = RedditCommentLoader.readData(data, comment -> comment.body.contains("BMW"));

            // System.out.println(comments.size());



            for (Comment comment : comments) {
                sentimentAnalyzer.setInputString(comment.body);
                sentimentAnalyzer.setInputStringProperties();
                sentimentAnalyzer.analyze();

                Map<String, Float> inputStringPolarity = sentimentAnalyzer.getPolarity();
                float commentCompoundScore = inputStringPolarity.get(ScoreType.COMPOUND);

                totalCompoundScore += commentCompoundScore;
            }

            System.out.println("average compound score: " + totalCompoundScore / comments.size());
        }
        catch (IOException e){
            System.out.println(e.toString());
        }
        return totalCompoundScore;
    }
}

