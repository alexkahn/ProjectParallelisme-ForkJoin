package be.vub.parallellism.solutions;

import java.util.concurrent.RecursiveTask;
import be.vub.parallellism.data.models.Comment;
import be.vub.parallellism.data.readers.RedditCommentLoader;
import com.vader.sentiment.analyzer.SentimentAnalyzer;
import com.vader.sentiment.util.ScoreType;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.sun.corba.se.spi.activation._ActivatorImplBase;

public class AnalyserTask extends RecursiveTask<Float>{

    private static final long serialVersionUID = 1L;

    SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();


    List<Comment> comments;
    Comment comment;
    float score;
    int first;
    int last;

    AnalyserTask(List<Comment> comments){
        this(comments, 0, comments.size());
    }

    AnalyserTask(List<Comment> comments, int first, int last){
        this.comments = comments;
        this.first = first;
        this.last = last;
    }

    @Override
    protected Float compute(){
        if (last - first < 2){
            try {
                return analyse(comments.get(first));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int pivot = (first + last)/2;
        AnalyserTask left_task = new AnalyserTask(comments, first, pivot);
        AnalyserTask right_task = new AnalyserTask(comments, pivot, last);
        right_task.fork();
        float left_score = left_task.compute();
        float right_score = right_task.join();

        return left_score + right_score;

    }

    private Float analyse(Comment comment) throws IOException {
//        System.out.println(comment);
        sentimentAnalyzer.setInputString(comment.body);
        sentimentAnalyzer.setInputStringProperties();
        sentimentAnalyzer.analyze();

        Map<String, Float> inputStringPolarity = sentimentAnalyzer.getPolarity();
        float commentCompoundScore = inputStringPolarity.get(ScoreType.COMPOUND);
//        System.out.println(commentCompoundScore);
        return commentCompoundScore / comments.size();
    }


}