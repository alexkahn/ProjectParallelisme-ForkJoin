package be.vub.parallellism.solutions;

import be.vub.parallellism.data.models.Comment;
import com.vader.sentiment.analyzer.SentimentAnalyzer;

import java.util.List;
import java.util.concurrent.RecursiveTask;

public class BrandAnalyserFilterTask extends RecursiveTask<Boolean> {
    private static final long serialVersionUID = 1L;

    private int counter = 0;

    SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();

    String commentBody;
    CharSequence brand;
    float score;
    int first;
    int last;
    int T;

    BrandAnalyserFilterTask(String commentBody, CharSequence brand){
        this(commentBody, 0, commentBody.length(), brand);
    }

    BrandAnalyserFilterTask(String commentBody, int first, int last, CharSequence brand){
        this.commentBody = commentBody;
        this.brand = brand;
        this.first = first;
        this.last = last;
        this.T = brand.length();
    }

    @Override
    protected Boolean compute() {
        if (last - first == T){
            // See if it contains the word
            boolean res = true;
//            for (int i = 0; i < T; i++ ){
//                int index = first + i;
//                res = res && (commentBody.charAt(index) == brand.charAt(i));
////                System.out.println("Brand found");
//            }
            return commentBody.startsWith((String) brand, first);
        }
        else if (last - first < T){
            return false;
        }
        int pivot = (first + last) / 2;
        BrandAnalyserFilterTask left_task = new BrandAnalyserFilterTask(commentBody, first, pivot, brand);
        BrandAnalyserFilterTask right_task = new BrandAnalyserFilterTask(commentBody, pivot, last, brand);
        right_task.fork();
        boolean left_score = left_task.compute();
        boolean right_score = right_task.join();

        return left_score || right_score;
    }
}
