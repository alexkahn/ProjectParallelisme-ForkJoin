package be.vub.parallellism.solutions;

import be.vub.parallellism.data.models.Comment;
import com.vader.sentiment.analyzer.SentimentAnalyzer;
import com.vader.sentiment.util.ScoreType;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class BrandAnalyserTask extends RecursiveTask<Float> {
    private static final long serialVersionUID = 1L;

    private int counter = 0;

    SentimentAnalyzer sentimentAnalyzer = new SentimentAnalyzer();


    List<Comment> comments;
    Comment comment;
    CharSequence brand;
    float score;
    int first;
    int last;
    int T = 2;

    BrandAnalyserTask(List<Comment> comments, CharSequence brand){
        this(comments, 0, comments.size(), brand);
    }

    BrandAnalyserTask(List<Comment> comments, int first, int last, CharSequence brand){
        this.comments = comments;
        this.brand = brand;
        this.first = first;
        this.last = last;
    }

    @Override
    protected Float compute(){
        if (last - first < T){
            try {
                if (match(comments.get(first))) {
                    counter++;
                    return analyse(comments.get(first));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int pivot = (first + last)/2;
        BrandAnalyserTask left_task = new BrandAnalyserTask(comments, first, pivot, brand);
        BrandAnalyserTask right_task = new BrandAnalyserTask(comments, pivot, last, brand);
        right_task.fork();
        float left_score = left_task.compute();
        float right_score = right_task.join();

        return (left_score + right_score);

    }

    private Float analyse(Comment comment) throws IOException {
//        System.out.println(comment);
        sentimentAnalyzer.setInputString(comment.body);
        sentimentAnalyzer.setInputStringProperties();
        sentimentAnalyzer.analyze();

        Map<String, Float> inputStringPolarity = sentimentAnalyzer.getPolarity();
        float commentCompoundScore = inputStringPolarity.get(ScoreType.COMPOUND);
        System.out.println(commentCompoundScore);
        return commentCompoundScore / counter;
    }

    private Boolean match(Comment comment){
        final int p = 2; // only 4 threads on my laptop

        ForkJoinPool pool = new ForkJoinPool(p);
        boolean res = pool.invoke(new BrandAnalyserFilterTask(comment.body, "BMW"));
        return res;
        //return comment.body.contains(brand);
    }

}
