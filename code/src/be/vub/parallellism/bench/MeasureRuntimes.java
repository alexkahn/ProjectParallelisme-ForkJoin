package be.vub.parallellism.bench;

import be.vub.parallellism.data.models.Comment;
import be.vub.parallellism.data.*;
import be.vub.parallellism.data.readers.RedditCommentLoader;
import be.vub.parallellism.solutions.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;


public class MeasureRuntimes {

	public static List<Comment> data;


	//results are stored to keep the compiler from optimising the code obtaining them away
	public static List<Float> use_results = new ArrayList<Float>(10000);

	//Identifiers for different experimental setups
	public enum Strategy{
		SEQ, 	//Sequential implementation
		PAR1,
		PAR2,
		PAR4,
		PAR8,
		PAR16,
		PAR32,
		PAR64,
		FILT1,
		FILT2,
		FILT4,
		FILT8,
		FILT16,
		FILT32,
		FILT64,


	}

	//Actual instantiations of the different strategies compared
	static final List<Function<List<Comment>,Float>> strategies =
			Arrays.asList(
					sequential(),
					parallelRun(1),
					parallelRun(2),
					parallelRun(4),
					parallelRun(8),
					parallelRun(16),
					parallelRun(32),
					parallelRun(64),
					parrallelFilterRun(1),
					parrallelFilterRun(2),
					parrallelFilterRun(4),
					parrallelFilterRun(8),
					parrallelFilterRun(16),
					parrallelFilterRun(32),
					parrallelFilterRun(64)
					);

	/*
	 * main method, to be used to perform run time measurements
	 *
	 * command-line args:
	 * args[0]: # repetitions to perform
	 * args[1]: # which dataset to use
	 * args[2-16] (optional): indicate for which presets to measure run times (default: 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15).
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Please provide '# repetitions to perform' as commandline argument.");
			return;
		}
		int n_repetitions = Integer.parseInt(args[0]);
		System.out.println("Repeating each measurement " + n_repetitions + " times.");

		List<Integer> preset_indices;
		if (args.length == 2) {
			preset_indices = Arrays.asList(1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15);
		}
		else {
			preset_indices = new ArrayList<Integer>(args.length - 1);
			for (int i = 2; i < args.length; i++) {
				preset_indices.add(Integer.parseInt(args[i]));
			}
		}
		String dataLocation = args[1];


		try {
			for (int i : preset_indices) {
				File res_file = new File("runtimes_" + i + ".csv");
				if (res_file.exists()) {
					//avoids accidentally overwriting previous results
					System.out.println(res_file + " already exists, skipping preset " + i);
					continue;
				}
				data = RedditCommentLoader.readData(dataLocation);
				System.out.println("Measuring runtimes for dataset " + i);
				for (int j = 0; j < strategies.size(); j++) {
					System.out.print(Strategy.values()[j] + ": ");
					System.out.flush();
					//measure runtimes
					List<Long> rts = benchmark(data, strategies.get(j), n_repetitions);
					//write to file
					write2file(res_file, Strategy.values()[j], rts);
					System.out.println("v");
				}
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	//writes runtimes for a given strategy to file
	static private void write2file(File f, Strategy s, List<Long> runtimes){
		PrintWriter csv_writer;
		try {
			csv_writer = new PrintWriter(new FileOutputStream(f,true));
			String line = ""+s;
			for(Long rt : runtimes){
				line += ","+rt;
			}
			csv_writer.println(line);
			csv_writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Measures the runtime(s) (in nanoseconds) for a given strategy on a given data preset for a given number of repetitions.
	 *
	 * @param preset: The data set to be filtered
	 * @param strategy: The filtering strategy, i.e. a function mapping data to count
	 * @param nrep: The number of times the experiment should be repeated consecutively
	 * @return A list of nrep runtimes (in nanoseconds), where the i'th element is the runtime of the i'th repetition
	 */
	static public List<Long> benchmark(List<Comment> preset, Function<List<Comment>,Float> strategy, int nrep){
		List<Long> runtimes = new ArrayList<Long>(nrep);
		for(int i = 0; i < nrep; i++){
			System.gc(); //a heuristic to avoid Garbage Collection (GC) to take place in timed portion of the code
			long before = System.nanoTime(); //time is measured in ns
			use_results.add(strategy.apply(preset)); //store results to avoid code getting optimised away
			runtimes.add(System.nanoTime()-before);
		}
		return runtimes;
	}

	/**
	 * @return The sequential filtering strategy
	 */
	static Function<List<Comment>,Float> sequential(){
		System.out.println("SEQUENTIAL:");
		return (List<Comment> data) -> {
			return SequentialAnalyser.analyse(data);
		};
	}

	/**
	 * @param p: number of worker threads
	 * @return The fine static partitioning strategy, using p worker threads
	 */
	static Function<List<Comment>,Float> parallelRun(int p){
		return (List<Comment> data) -> {
			return ParallelAnalyser.analyse(data, p);
		};
	}

	/**
	 * @param p: number of worker threads
	 * @return The fork-join strategy splitting up on region level, using p worker threads
	 */
	static Function<List<Comment>,Float> parrallelFilterRun(int p){
		return (List<Comment> data) -> {
			return ParallelFilterAnalyser.analyse(data, p);
		};
	}
}
